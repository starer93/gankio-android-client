package com.developer.yu.gankio.base

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import java.util.ArrayList

abstract class BaseAdapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private val t = ArrayList<T>()
    protected abstract val itemLayout: Int

    constructor() : super()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(itemLayout, parent, false)
        return getViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        onBindView(holder, t[position])
    }

    override fun getItemCount(): Int {
        return t.size
    }

    protected abstract fun onBindView(holder: RecyclerView.ViewHolder, t: T)
    protected abstract fun getViewHolder(view: View): RecyclerView.ViewHolder
}
