package com.developer.yu.gankio.pojo

import com.google.gson.annotations.SerializedName

class Feed
{
    @SerializedName("_id")
    var id = ""
    var createdAt = ""
    var desc = ""
    var publishedAt = ""
    var source =""
    var type = ""
    var url =""
    var used = true
    var who = null

}
